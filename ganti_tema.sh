#!/bin/bash
# Bash Menu To change theme

# Butuh user-theme
# sudo cp $HOME/.local/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com/schemas/org.gnome.shell.extensions.user-theme.gschema.xml /usr/share/glib-2.0/schemas
# sudo glib-compile-schemas /usr/share/glib-2.0/schemas

# Butuh unite
# sudo cp $HOME/.local/share/gnome-shell/extensions/unite@hardpixel.eu/schemas/org.gnome.shell.extensions.unite.gschema.xml /usr/share/glib-2.0/schemas
# sudo glib-compile-schemas /usr/share/glib-2.0/schemas

clear
PS3='Silahkan Masukkan Pilihan Anda: '

# Set panel ke top; 
toppanel() {
    echo "Set top panel"
    gsettings set org.gnome.shell enabled-extensions "['bolt@zacbarton.com', 'gnomeGlobalAppMenu@lestcape', 'weather-extension@xeked.com', 'show-ip@sgaraud.github.com', 'systeminfo@noctrl.eu', 'System_Monitor@bghome.gmail.com', 'system-monitor-in-usermenu@arthur.net', 'systemMonitor@gnome-shell-extensions.gcampax.github.com', 'easy_docker_containers@red.software.systems', 'add-username-toppanel@brendaw.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'sound-output-device-chooser@kgshank.net', 'ubuntu-appindicators@ubuntu.com', 'caffeine@patapon.info', 'extensions@abteil.org',  'user-theme', 'multi-monitors-add-on@spin83', 'docker_status@gpouilloux',  'alternate-tab@gnome-shell-extensions.gcampax.github.com', 'Move_Clock@rmy.pobox.com', 'workspace-indicator@gnome-shell-extensions.gcampax.github.com']" ;
    
    echo "Set button layout"
    gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:menu'    
    killall plank
}

toppanelunite() {
    echo "Set top panel"
    gsettings set org.gnome.shell enabled-extensions "['bolt@zacbarton.com', 'gnomeGlobalAppMenu@lestcape', 'weather-extension@xeked.com', 'show-ip@sgaraud.github.com', 'systeminfo@noctrl.eu', 'System_Monitor@bghome.gmail.com', 'system-monitor-in-usermenu@arthur.net', 'systemMonitor@gnome-shell-extensions.gcampax.github.com', 'easy_docker_containers@red.software.systems', 'add-username-toppanel@brendaw.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'sound-output-device-chooser@kgshank.net', 'ubuntu-appindicators@ubuntu.com', 'caffeine@patapon.info', 'extensions@abteil.org',  'user-theme', 'docker_status@gpouilloux',  'alternate-tab@gnome-shell-extensions.gcampax.github.com','unite@hardpixel.eu', 'workspace-indicator@gnome-shell-extensions.gcampax.github.com']" ;
    
    echo "Set button layout"
    gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:menu'    
    killall plank
}

# SideDash
sidedash(){
    echo "Set Side Dash"
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-position LEFT
    gsettings set org.gnome.shell.extensions.dash-to-dock extend-height 'true'
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed 'true'
    gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 24
    gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-favorites true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-running true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-show-apps-button true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-windows-preview true
}

# BottomDash
bottomdash(){
    echo "Set Bottom Dash"
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
    gsettings set org.gnome.shell.extensions.dash-to-dock extend-height 'false'
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed 'false'
    gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 36
    gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor false
    gsettings set org.gnome.shell.extensions.dash-to-dock show-favorites true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-running true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-show-apps-button true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-windows-preview true
}

# BottomDash
bottomdashelementary(){
    echo "Set Bottom Dash"
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
    gsettings set org.gnome.shell.extensions.dash-to-dock extend-height 'false'
    gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed 'false'
    gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 36
    gsettings set org.gnome.shell.extensions.dash-to-dock multi-monitor true
    gsettings set org.gnome.shell.extensions.dash-to-dock show-favorites false
    gsettings set org.gnome.shell.extensions.dash-to-dock show-running false
    gsettings set org.gnome.shell.extensions.dash-to-dock show-show-apps-button false
    gsettings set org.gnome.shell.extensions.dash-to-dock show-windows-preview false

    plank &
}

# Set panel ke bottom;
bottompanel(){
    echo "Set bottom panel"
    gsettings set org.gnome.shell enabled-extensions "['bolt@zacbarton.com', 'gnomeGlobalAppMenu@lestcape', 'weather-extension@xeked.com', 'show-ip@sgaraud.github.com', 'systeminfo@noctrl.eu', 'System_Monitor@bghome.gmail.com', 'system-monitor-in-usermenu@arthur.net', 'systemMonitor@gnome-shell-extensions.gcampax.github.com', 'easy_docker_containers@red.software.systems', 'add-username-toppanel@brendaw.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'sound-output-device-chooser@kgshank.net', 'ubuntu-appindicators@ubuntu.com', 'caffeine@patapon.info', 'extensions@abteil.org',  'user-theme', 'dash-to-panel@jderose9.github.com', 'arc-menu@linxgem33.com', 'docker_status@gpouilloux',  'alternate-tab@gnome-shell-extensions.gcampax.github.com', 'workspace-indicator@gnome-shell-extensions.gcampax.github.com']"
    # gsettings set org.gnome.shell.extensions.dash-to-panel panel-size 32

    echo "Set button layout"
    gsettings set org.gnome.desktop.wm.preferences button-layout 'menu:minimize,maximize,close'
    killall plank
}

info(){
    echo "Anda memilih $REPLY => $opt"
    # neofetch
}

restartgnomeshell(){
    echo "Jika tidak ada perubahan, restart gnome shell.";
    echo "Tekan Alt+ f2, lalu ketik r. Enter";
}

options=(
    "Ubuntu-18-04-LTS" 
    "Ubuntu-18-04-LTS-Bottom-Panel"
    "Ubuntu-18-04-LTS-Bottom-Dash"
    # "Ubuntu-18-04-LTS-Bottom-Dash-Elementary"
    "Ubuntu-19-10-light" 
    "Ubuntu-19-10-dark" 
    "MacOs Sierra Light" 
    "MacOs Sierra Dark" 
    "Mac Mojave"
    "Dark Elegant"
    "Windows XP" 
    "Windows 7" 
    "Windows 10" 
    "Marwaita" 
    "Marwaita-Dark"
    "Arrongin" 
    # "Yaru-Red"
    # "Yaru-Green-Dark"
    # "Parrot-OS" 
    # "Elementary-OS"
    # "Material-Solarized"
    # "Orchis"
    # "Orchis-Dark"
    # "Sunset-Easy-Ke-Mata"
    "Dzil-Custom-Green"
    "Quit"
)

# echo "Note:"
# echo "Pastikan gnome-extension berikut ini sudah terpasang:"
# echo "1. user-theme"
# echo ""

echo "+----------------------------------------------------------------------------+";
echo "| Menu Mengganti Tema Ubuntu.                                                |";
echo "| Gnome Extension: user-theme harus terpasang                        By Dzil |";
echo "+----------------------------------------------------------------------------+";

select opt in "${options[@]}"
do
    case $opt in
        "Ubuntu-18-04-LTS")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Ambiance";
            gsettings set org.gnome.desktop.interface icon-theme 'ubuntu-mono-dark';
            gsettings set org.gnome.shell.extensions.user-theme name 'Adwaita'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/warty-final-ubuntu.png'

            sidedash;
            info;
            
            break
            ;;

        "Ubuntu-18-04-LTS-Bottom-Panel")
            bottompanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Ambiance";
            gsettings set org.gnome.desktop.interface icon-theme 'ubuntu-mono-dark';
            gsettings set org.gnome.shell.extensions.user-theme name "Ambiance-Gnome"
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/warty-final-ubuntu.png'
        
            info;
            break
            ;;

        "Ubuntu-18-04-LTS-Bottom-Dash")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Ambiance";
            gsettings set org.gnome.desktop.interface icon-theme 'ubuntu-mono-dark';
            gsettings set org.gnome.shell.extensions.user-theme name 'Adwaita'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/warty-final-ubuntu.png'

            bottomdash;
            info;
            
            break
            ;;

        "Ubuntu-18-04-LTS-Bottom-Dash-Elementary")
            toppanelunite;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Ambiance";
            gsettings set org.gnome.desktop.interface icon-theme 'ubuntu-mono-dark';
            gsettings set org.gnome.shell.extensions.user-theme name 'Adwaita'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/warty-final-ubuntu.png'

            bottomdash;
            info;
            
            break
            ;;


         "Dark Elegant")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Shades-of-gray-Arch";
            gsettings set org.gnome.desktop.interface icon-theme 'Flat-Remix-Blue';
            gsettings set org.gnome.shell.extensions.user-theme name 'Minimal-Conception'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Dzil-Custom/dark-elegant.jpg'

            bottomdash;
            info;
            
            break
            ;;

         "Mac Mojave")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Mojave-dark";
            gsettings set org.gnome.desktop.interface icon-theme 'McMojave-circle';
            gsettings set org.gnome.shell.extensions.user-theme name 'Emerald Shell'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Dzil-Custom/dark-elegant.jpg'

            sidedash;
            info;
            
            break
            ;;

        "Ubuntu-19-10-light")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "United-Ubuntu-Compact";
            gsettings set org.gnome.desktop.interface icon-theme 'Arc-Ubuntu';
            gsettings set org.gnome.shell.extensions.user-theme name 'United-Ubuntu-Compact'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-Black'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/warty-final-ubuntu.png'
            
            sidedash;
            info;
            
            break
            ;;

        "Ubuntu-19-10-dark")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "United-Ubuntu-Dark-Compact";
            gsettings set org.gnome.desktop.interface icon-theme 'Arc-Ubuntu';
            gsettings set org.gnome.shell.extensions.user-theme name 'United-Ubuntu-Compact'
            gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/warty-final-ubuntu.png'

            sidedash;
            info;

            break
            ;;

        "MacOs Sierra Light")
            toppanel;            

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Sierra-compact-light";
            gsettings set org.gnome.desktop.interface icon-theme 'Mojave-CT-Eos';
            gsettings set org.gnome.shell.extensions.user-theme name 'macOS-5.2'
            gsettings set org.gnome.desktop.interface cursor-theme 'McMojave-cursors'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/MacOS-Sierra/bg.jpg'
            
            bottomdash;
            info;
            break
            ;;

        "MacOs Sierra Dark")
            toppanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Sierra-compact-dark";
            gsettings set org.gnome.desktop.interface icon-theme 'Mojave-CT-Eos';
            gsettings set org.gnome.shell.extensions.user-theme name 'macOS-Dark-5.2-dark'
            gsettings set org.gnome.desktop.interface cursor-theme 'McMojave-cursors'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/MacOS-Sierra/bg.jpg'
            
            bottomdash;
            info;
            break
            ;;

        "Windows XP")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Windows XP Luna";
            gsettings set org.gnome.desktop.interface icon-theme 'Windows-XP-3.1';
            gsettings set org.gnome.shell.extensions.user-theme name 'Windows XP Luna'
            gsettings set org.gnome.desktop.interface cursor-theme 'win8'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Windows-XP/bg.jpg'
            
            info;
            break
            ;;

        "Windows 7")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Windows-7-2.1";
            gsettings set org.gnome.desktop.interface icon-theme 'Windows-7-1.0';
            gsettings set org.gnome.shell.extensions.user-theme name 'Windows-7-2.1'
            gsettings set org.gnome.desktop.interface cursor-theme 'win8'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Windows/wallhaven-411127.jpg'
            
            info;
            break
            ;;

        "Windows 10")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Windows10";
            gsettings set org.gnome.desktop.interface icon-theme 'Windows-10-master';
            gsettings set org.gnome.shell.extensions.user-theme name 'Windows-10-3.2';
            gsettings set org.gnome.desktop.interface cursor-theme 'win8';
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Windows/wallhaven-411127.jpg'
            
            info;
            break
            ;;
        
        "Marwaita")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Marwaita Ubuntu";
            gsettings set org.gnome.desktop.interface icon-theme 'Marwaita';
            gsettings set org.gnome.shell.extensions.user-theme name 'Marwaita Ubuntu'
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';
            
            info;
            break
            ;;

        "Marwaita-Dark")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Marwaita Ubuntu Dark";
            gsettings set org.gnome.desktop.interface icon-theme 'Marwaita';
            gsettings set org.gnome.shell.extensions.user-theme name 'Marwaita Ubuntu';
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';
            
            info;
            break
            ;;

        "Yaru-Red")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Yaru-Red";
            gsettings set org.gnome.desktop.interface icon-theme 'Yaru-Red';
            gsettings set org.gnome.shell.extensions.user-theme name 'Yaru-Red';
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';
            
            info;
            break;;

        "Yaru-Green-Dark")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Yaru-Green-dark";
            gsettings set org.gnome.desktop.interface icon-theme 'Yaru-Green';
            gsettings set org.gnome.shell.extensions.user-theme name 'Yaru-Green';
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';
            
            info;
            break
            ;;

        "Parrot-OS")
            toppanel;
        
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Arc-Black-Pistachio";
            gsettings set org.gnome.desktop.interface icon-theme 'Tela-manjaro';
            gsettings set org.gnome.shell.extensions.user-theme name 'Arc-Black-Pistachio';
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';

            sidedash;
            info;
            
            break
            ;;   

        "Arrongin")
            toppanel;
            
            # echo 'Set Unite'
            # gsettings set org.gnome.shell.extensions.unite window-buttons-theme "arrongin"
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Arrongin-Buttons-Left";
            gsettings set org.gnome.desktop.interface icon-theme 'Arrongin-icon-theme';
            gsettings set org.gnome.shell.extensions.user-theme name 'Arrongin-Buttons-Left';
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';

            bottomdash;
            info;
            
            break
            ;;   

        "Elementary-OS")
            toppanel;
            
            # echo 'Set Unite'
            # gsettings set org.gnome.shell.extensions.unite window-buttons-theme "arrongin"
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Marwaita Ubuntu";
            gsettings set org.gnome.desktop.interface icon-theme 'elementary-p';
            gsettings set org.gnome.shell.extensions.user-theme name 'United-Ubuntu-alt-Compact';
            gsettings set org.gnome.desktop.interface cursor-theme 'Marwaita-Light';
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Elementary-OS/bg.jpg'

            bottomdashelementary;
            info;
            
            break;;   

        


        "Material-Solarized")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Material-Solarized-GTK";
            gsettings set org.gnome.desktop.interface icon-theme 'Material-Solarized-Suru';
            gsettings set org.gnome.shell.extensions.user-theme name 'Material-Solarized-GTK'
            gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Material-Solarized/wallhaven-477m53.png'
            gsettings set org.gnome.desktop.interface cursor-theme 'Cyan-Cursor'

            sidedash;

            info;
            
            break
            ;;

        "Orchis")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Orchis-compact";
            gsettings set org.gnome.desktop.interface icon-theme 'Tela';
            gsettings set org.gnome.shell.extensions.user-theme name 'Orchis-compact'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Material-Solarized/wallhaven-477m53.png'
            gsettings set org.gnome.desktop.interface cursor-theme 'McMojave-cursors'

            bottomdash;

            info;
            
            break
            ;;           

        "Orchis-Dark")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Orchis-dark-compact";
            gsettings set org.gnome.desktop.interface icon-theme 'Tela';
            gsettings set org.gnome.shell.extensions.user-theme name 'Orchis-dark-compact'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Material-Solarized/wallhaven-477m53.png'
            gsettings set org.gnome.desktop.interface cursor-theme 'McMojave-cursors'

            bottomdash;

            info;
            
            break
            ;;  

        "Sunset-Easy-Ke-Mata")
            toppanel;
            
            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "SunSet-Dark";
            gsettings set org.gnome.desktop.interface icon-theme 'Material-Black-Cherry-Numix-FLAT';
            gsettings set org.gnome.shell.extensions.user-theme name 'United-Ubuntu-alt-Compact'
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Sunset/bg.jpg'
            gsettings set org.gnome.desktop.interface cursor-theme 'Deepin-4All'

            bottomdash;

            info;
            
            break
            ;;  

        "Dzil-Custom-Green")
            bottompanel;

            #echo 'Set Theme'
            gsettings set org.gnome.desktop.interface gtk-theme "Arc-Black-Pistachio";
            gsettings set org.gnome.desktop.interface icon-theme 'Tela-manjaro-dark';
            gsettings set org.gnome.shell.extensions.user-theme name 'Yaru-Green-dark';
            gsettings set org.gnome.desktop.interface cursor-theme 'Deepin-4All';
            # gsettings set org.gnome.desktop.background picture-uri 'file:///home/dzil/Pictures/theming/Dzil-Custom/1.jpg'
            
            info;
            break
            ;;
        
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

restartgnomeshell;
